/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.test.services.impl;

import javax.enterprise.context.ApplicationScoped;

import org.eclipsefoundation.openvsx.services.OAuthService;

import io.quarkus.test.Mock;

@Mock
@ApplicationScoped
public class MockOAuthService implements OAuthService {

    @Override
    public String getToken() {
        return "";
    }
}
