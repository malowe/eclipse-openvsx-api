/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.services.impl;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.foundationdb.client.model.PeopleData;
import org.eclipsefoundation.foundationdb.client.model.PeopleDocumentData;
import org.eclipsefoundation.foundationdb.client.model.SysModLogData;
import org.eclipsefoundation.openvsx.api.PeopleAPI;
import org.eclipsefoundation.openvsx.api.SysAPI;
import org.eclipsefoundation.openvsx.api.models.EfUser;
import org.eclipsefoundation.openvsx.api.models.ModLogHeaders;
import org.eclipsefoundation.openvsx.namespace.OpenvsxModLogActions;
import org.eclipsefoundation.openvsx.services.FoundationOperationService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
public class DefaultFoundationOperationService implements FoundationOperationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultFoundationOperationService.class);

    @ConfigProperty(name = "eclipse.openvsx.doc-id")
    String openvsxDocId;

    @Inject
    @RestClient
    PeopleAPI peopleAPI;
    @Inject
    @RestClient
    SysAPI sysAPI;

    @Inject
    CachingService cache;
    @Inject
    RequestWrapper wrap;

    @Override
    public Optional<PeopleDocumentData> fetchMostRecentDocument(String username) {
        try {
            LOGGER.debug("Fetching most recent publisher agreement for user: {}", username);

            List<PeopleDocumentData> results = peopleAPI.getPeopleDocument(username, openvsxDocId);
            if (results == null || results.isEmpty()) {
                LOGGER.warn("Unable to find agreement for user with name: {}", username);
                return Optional.empty();
            }

            // Sort by date. Most recent first
            return results.stream()
                    .sorted((pDoc1, pDoc2) -> pDoc2.getEffectiveDate().compareTo(pDoc1.getEffectiveDate()))
                    .findFirst();

        } catch (Exception e) {
            LOGGER.error("Error while fetching publisher agreement for user: {}", username, e);
            return Optional.empty();
        }
    }

    @Override
    public Optional<PeopleDocumentData> persistDocumentWithModLog(ModLogHeaders headers, PeopleDocumentData toInsert) {
        LOGGER.debug("Persisting publisher agreement for user: {}", toInsert.getPersonID());

        List<PeopleDocumentData> results = peopleAPI.persistPeopleDocument(headers, toInsert.getPersonID(), toInsert);
        if (results == null || results.isEmpty()) {
            LOGGER.error("Unable to create agreement for user with name: {}", toInsert.getPersonID());
            return Optional.empty();
        }

        return Optional.of(results.get(0));
    }

    @Override
    public Optional<SysModLogData> insertErrorSysModLog(String pk1, String personId) {
        try {
            // All error ModLogs use client IP and log the failed action taken
            SysModLogData src = SysModLogData.builder()
                    .setLogTable("openvsx/publisher_agreement")
                    .setPK1(pk1)
                    .setPK2(getBestMatchingIP())
                    .setLogAction(OpenvsxModLogActions.SQL_ERROR)
                    .setPersonId(personId)
                    .setModDateTime(ZonedDateTime.now())
                    .build();

            LOGGER.debug("Inserting error ModLog: {}", src);

            List<SysModLogData> results = sysAPI.insertSysModLog(src);
            if (results == null || results.isEmpty()) {
                LOGGER.error("Error while inserting error ModLog: {}", src);
                return Optional.empty();
            }

            return Optional.of(results.get(0));
        } catch (Exception e) {
            LOGGER.error("Error persisting ModLog", e);
            return Optional.empty();
        }
    }

    @Override
    public boolean createDbUserIfNotFound(EfUser user) {
        try {
            // No need to create new user if they already exist in fdndb
            Optional<PeopleData> fetchResult = fetchFoundationUser(user.getName());
            if (fetchResult.isPresent()) {
                return true;
            }

            LOGGER.debug("User {} not found in fdndb. Creating new entry", user.getName());

            PeopleData person = PeopleData.builder()
                    .setPersonID(user.getName())
                    .setFname(user.getFirstName())
                    .setLname(user.getLastName())
                    .setType("XX")
                    .setMember(false)
                    .setEmail(user.getMail())
                    .setUnixAcctCreated(false)
                    .setIssuesPending(false)
                    .build();

            // Insert new person record
            return peopleAPI.persistPersonEntity(person).getStatus() == 200;

        } catch (Exception e) {
            LOGGER.error("Error inserting user: {}", user.getName(), e);
            insertErrorSysModLog("createFoundationUser", user.getName());
            return false;
        }
    }

    /**
     * Fetches a user given a specific personId and returns an Optional containing
     * the result if it exists.
     * 
     * @param personId The desired person's ID
     * @return an Optional containing the desired PeopleData if it exists.
     */
    private Optional<PeopleData> fetchFoundationUser(String personId) {
        try {
            LOGGER.debug("Fetching user: {}", personId);
            return cache.get(personId, new MultivaluedMapImpl<>(), PeopleData.class,
                    () -> peopleAPI.getPerson(personId));
        } catch (Exception e) {
            LOGGER.error("Error searching for user: {}", personId, e);
            insertErrorSysModLog("fetchFoundationUser", personId);
            return Optional.empty();
        }
    }

    /**
     * Retrieves the best IP for the current request, using the x-real-ip and
     * x-forwarded-for headers to pull the
     * information from the current request.
     * 
     * @return the best matching IP for current request, or localhost if none can be
     *         found
     */
    private String getBestMatchingIP() {
        LOGGER.debug("Looking up best IP address");
        // use real IP, followed by forwarded for to get the requests IP
        String initialIp = wrap.getHeader("X-Real-Ip");
        if (StringUtils.isBlank(initialIp)) {
            String[] forwardedIps = StringUtils.split(wrap.getHeader("X-Forwarded-For"), ",\\s*");
            if (forwardedIps != null && forwardedIps.length > 1) {
                initialIp = forwardedIps[1];
            }
        }
        // default to localhost if we can't find request information
        if (StringUtils.isBlank(initialIp)) {
            initialIp = "0.0.0.0";
        }
        return initialIp;
    }

}
