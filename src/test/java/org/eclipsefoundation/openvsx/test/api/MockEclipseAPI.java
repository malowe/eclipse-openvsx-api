/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.openvsx.api.EclipseAPI;
import org.eclipsefoundation.openvsx.api.models.EfUser;
import org.eclipsefoundation.openvsx.api.models.EfUser.Country;

import io.quarkus.test.Mock;

@Mock
@RestClient
@ApplicationScoped
public class MockEclipseAPI implements EclipseAPI {

    private List<EfUser> users;

    public MockEclipseAPI() {
        this.users = new ArrayList<>();
        this.users.addAll(Arrays.asList(
                EfUser.builder()
                        .setUid(666)
                        .setName("firstlast")
                        .setGithubHandle("handle")
                        .setMail("Mail@test.com")
                        .setPicture("pic url")
                        .setFirstName("fake")
                        .setLastName("user")
                        .setPublisherAgreements(new HashMap<>())
                        .setTwitterHandle("")
                        .setOrg("null")
                        .setJobTitle("employee")
                        .setWebsite("site url")
                        .setCountry(Country.builder().build())
                        .setInterests(Arrays.asList())
                        .setWorkingGroupsInterests(Arrays.asList())
                        .build(),
                EfUser.builder()
                        .setUid(42)
                        .setName("fakeuser")
                        .setPicture("pic url")
                        .setFirstName("fake")
                        .setLastName("user")
                        .setMail("Mail@test.com")
                        .setPublisherAgreements(new HashMap<>())
                        .setGithubHandle("fakeuser")
                        .setTwitterHandle("")
                        .setOrg("null")
                        .setJobTitle("employee")
                        .setWebsite("site url")
                        .setCountry(Country.builder().build())
                        .setInterests(Arrays.asList())
                        .setWorkingGroupsInterests(Arrays.asList())
                        .build(),
                EfUser.builder()
                        .setUid(333)
                        .setName("name")
                        .setGithubHandle("name")
                        .setMail("Mail@test.com")
                        .setPicture("pic url")
                        .setFirstName("fake")
                        .setLastName("user")
                        .setPublisherAgreements(new HashMap<>())
                        .setTwitterHandle("")
                        .setOrg("null")
                        .setJobTitle("employee")
                        .setWebsite("site url")
                        .setCountry(Country.builder().build())
                        .setInterests(Arrays.asList())
                        .setWorkingGroupsInterests(Arrays.asList())
                        .build(),
                EfUser.builder()
                        .setUid(11)
                        .setName("testtesterson")
                        .setGithubHandle("mctesty")
                        .setMail("Mail@test.com")
                        .setPicture("pic url")
                        .setFirstName("fake")
                        .setLastName("user")
                        .setPublisherAgreements(new HashMap<>())
                        .setTwitterHandle("")
                        .setOrg("null")
                        .setJobTitle("employee")
                        .setWebsite("site url")
                        .setCountry(Country.builder().build())
                        .setInterests(Arrays.asList())
                        .setWorkingGroupsInterests(Arrays.asList())
                        .build(),
                EfUser.builder()
                        .setUid(444)
                        .setName("nodoc")
                        .setGithubHandle("nodoc")
                        .setMail("Mail@test.com")
                        .setPicture("pic url")
                        .setFirstName("fake")
                        .setLastName("user")
                        .setPublisherAgreements(new HashMap<>())
                        .setTwitterHandle("")
                        .setOrg("null")
                        .setJobTitle("employee")
                        .setWebsite("site url")
                        .setCountry(Country.builder().build())
                        .setInterests(Arrays.asList())
                        .setWorkingGroupsInterests(Arrays.asList())
                        .build()));
    }

    @Override
    public List<EfUser> getUsers(String token, String uid) {
        return users.stream().filter(u -> u.getUid() == Integer.parseInt(uid)).collect(Collectors.toList());
    }
}
