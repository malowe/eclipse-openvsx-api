/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.api.models;

import javax.annotation.Nullable;
import javax.ws.rs.HeaderParam;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_ModLogHeaders.Builder.class)
public abstract class ModLogHeaders {

    @HeaderParam("X-ModLog-Key-1")
    public abstract String getModLogKey1();

    @Nullable
    @HeaderParam("X-ModLog-Key-2")
    public abstract String getModLogKey2();

    @HeaderParam("X-ModLog-table")
    public abstract String getModLogTable();

    @HeaderParam("X-ModLog-Person")
    public abstract String getModLogPerson();

    @Nullable
    @HeaderParam("X-ModLog-Action")
    public abstract String getModLogAction();

    @Nullable
    @HeaderParam("X-ModLog-Time")
    public abstract String getModLogTime();

    public static Builder builder() {
        return new AutoValue_ModLogHeaders.Builder()
                .setModLogTable("openvsx/publisher_agreement")
                .setModLogKey1("api.eclipse.org");
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setModLogKey1(String key);

        public abstract Builder setModLogKey2(String key);

        public abstract Builder setModLogTable(String table);

        public abstract Builder setModLogPerson(String person);

        public abstract Builder setModLogAction(String action);

        public abstract Builder setModLogTime(String time);

        public abstract ModLogHeaders build();
    }
}