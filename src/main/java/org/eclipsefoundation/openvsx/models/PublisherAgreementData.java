/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_PublisherAgreementData.Builder.class)
public abstract class PublisherAgreementData {

    @JsonProperty("PersonID")
    public abstract String getPersonID();

    @JsonProperty("DocumentID")
    public abstract String getDocumentID();

    @JsonProperty("Version")
    public abstract String getVersion();

    @JsonProperty("EffectiveDate")
    public abstract String getEffectiveDate();

    @JsonProperty("ReceivedDate")
    public abstract String getReceivedDate();

    @JsonProperty("ScannedDocumentBLOB")
    public abstract String getScannedDocumentBlob();

    @JsonProperty("ScannedDocumentMime")
    public abstract String getScannedDocumentMime();

    @JsonProperty("ScannedDocumentBytes")
    public abstract String getScannedDocumentBytes();

    @JsonProperty("ScannedDocumentFileName")
    public abstract String getScannedDocumentFileName();

    @JsonProperty("Comments")
    public abstract String getComments();

    public static Builder builder() {
        return new AutoValue_PublisherAgreementData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setPersonID(String id);

        public abstract Builder setDocumentID(String id);

        public abstract Builder setVersion(String version);

        public abstract Builder setEffectiveDate(String date);

        public abstract Builder setReceivedDate(String date);

        public abstract Builder setScannedDocumentBlob(String blob);

        public abstract Builder setScannedDocumentMime(String mime);

        public abstract Builder setScannedDocumentBytes(String bytes);

        public abstract Builder setScannedDocumentFileName(String name);

        public abstract Builder setComments(String commetns);

        public abstract PublisherAgreementData build();
    }
}
