/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.test.api;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.ArrayUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.foundationdb.client.model.PeopleData;
import org.eclipsefoundation.foundationdb.client.model.PeopleDocumentData;
import org.eclipsefoundation.openvsx.api.PeopleAPI;
import org.eclipsefoundation.openvsx.api.models.ModLogHeaders;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;

import io.quarkus.test.Mock;

@Mock
@RestClient
@ApplicationScoped
public class MockPeopleAPI implements PeopleAPI {

    private MultivaluedMap<String, PeopleDocumentData> peopleDocs;
    private Map<String, PeopleData> people;

    public MockPeopleAPI() {
        this.peopleDocs = new MultivaluedMapImpl<>();

        this.peopleDocs.add("fakeuser", buildPeopleDocument("fakeuser", "sampleId"));
        this.peopleDocs.add("firstlast", buildPeopleDocument("firstlast", "sampleId"));
        this.peopleDocs.add("name", buildPeopleDocument("name", "otherId"));
        this.peopleDocs.add("username", buildPeopleDocument("username", "sampleId"));

        this.people = new HashMap<>();
        this.people.put("fakeuser", buildPeople("fakeuser"));
        this.people.put("firstlast", buildPeople("firstlast"));
        this.people.put("name", buildPeople("name"));
        this.people.put("username", buildPeople("username"));
    }

    @Override
    public Response persistPersonEntity(PeopleData src) {
        return Response.ok(Arrays.asList(people.put(src.getPersonID(), src))).build();
    }

    @Override
    public PeopleData getPerson(String personId) {
        return people.get(personId);
    }

    @Override
    public List<PeopleDocumentData> getPeopleDocument(String personId, String documentId) {
        return peopleDocs.get(personId).isEmpty() ? Collections.emptyList()
                : peopleDocs.get(personId)
                        .stream()
                        .filter(pd -> pd.getDocumentID().equalsIgnoreCase(documentId))
                        .collect(Collectors.toList());
    }

    @Override
    public List<PeopleDocumentData> persistPeopleDocument(ModLogHeaders modLog, String personId,
            PeopleDocumentData src) {

        if (peopleDocs.get(personId) != null && !peopleDocs.get(personId).isEmpty()) {

            // Remove if exists. Then add it
            Optional<PeopleDocumentData> existingDoc = peopleDocs.get(personId).stream()
                    .filter(d -> d.getPersonID().equalsIgnoreCase(personId)
                            && d.getDocumentID().equalsIgnoreCase(src.getDocumentID())
                            && d.getEffectiveDate().compareTo(src.getEffectiveDate()) == 0)
                    .findFirst();
            if (existingDoc.isPresent()) {
                peopleDocs.remove(personId, existingDoc.get());
            }
        }

        peopleDocs.add(personId, src);

        return Arrays.asList(src);
    }

    private PeopleData buildPeople(String personId) {
        return PeopleData.builder()
                .setPersonID(personId)
                .setFname("test")
                .setLname("entity")
                .setType("XX")
                .setMember(false)
                .setEmail("test@mail.com")
                .setUnixAcctCreated(false)
                .setIssuesPending(false)
                .build();
    }

    private PeopleDocumentData buildPeopleDocument(String personId, String docId) {
        return PeopleDocumentData.builder()
                .setPersonID(personId)
                .setDocumentID(docId)
                .setVersion(1)
                .setEffectiveDate(new Date())
                .setReceivedDate(new Date())
                .setScannedDocumentFileName("name")
                .setScannedDocumentMime("application/json")
                .setScannedDocumentBLOB(Arrays.asList(ArrayUtils.toObject("test blob".getBytes())))
                .setScannedDocumentBytes(100)
                .setComments("Some comments")
                .build();
    }
}
