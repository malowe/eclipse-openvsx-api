/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.models;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_DocumentBody.Builder.class)
public abstract class DocumentBody {

    public abstract String getVersion();

    public abstract String getGithubHandle();

    public abstract String getUsername();

    public abstract String getFirstName();

    public abstract String getLastName();

    @Nullable
    public abstract String getMail();

    public static Builder builder() {
        return new AutoValue_DocumentBody.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setVersion(String version);

        public abstract Builder setGithubHandle(String handle);

        public abstract Builder setUsername(String name);

        public abstract Builder setFirstName(String name);

        public abstract Builder setLastName(String name);

        public abstract Builder setMail(String mail);

        public abstract DocumentBody build();
    }
}
