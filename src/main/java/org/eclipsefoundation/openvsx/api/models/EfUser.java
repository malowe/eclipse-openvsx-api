/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.api.models;

import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_EfUser.Builder.class)
public abstract class EfUser {

    public abstract Integer getUid();

    public abstract String getName();

    public abstract String getPicture();

    @Nullable
    public abstract String getMail();

    @Nullable
    public abstract Eca getEca();

    @Nullable
    public abstract Boolean getIsCommitter();

    @Nullable
    public abstract FriendsData getFriends();

    public abstract String getFirstName();

    public abstract String getLastName();

    public abstract Map<String, PublisherAgreement> getPublisherAgreements();

    @Nullable
    public abstract String getGithubHandle();

    public abstract String getTwitterHandle();

    public abstract String getOrg();

    @Nullable
    public abstract String getOrgId();

    public abstract String getJobTitle();

    public abstract String getWebsite();

    public abstract Country getCountry();

    @Nullable
    public abstract String getBio();

    public abstract List<String> getInterests();

    public abstract List<String> getWorkingGroupsInterests();

    @Nullable
    public abstract String getForumsUrl();

    @Nullable
    public abstract String getProjectsUrl();

    @Nullable
    public abstract String getGerritUrl();

    @Nullable
    public abstract String getMailinglistUrl();

    @Nullable
    public abstract String getMpcFavoritesUrl();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_EfUser.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setUid(Integer uid);

        public abstract Builder setName(String name);

        public abstract Builder setPicture(String picture);

        public abstract Builder setMail(@Nullable String mail);

        public abstract Builder setEca(@Nullable Eca eca);

        public abstract Builder setIsCommitter(@Nullable Boolean isCommitter);

        public abstract Builder setFriends(@Nullable FriendsData friends);

        public abstract Builder setFirstName(String fName);

        public abstract Builder setLastName(String lName);

        public abstract Builder setPublisherAgreements(Map<String, PublisherAgreement> agreements);

        public abstract Builder setGithubHandle(@Nullable String handle);

        public abstract Builder setTwitterHandle(String handle);

        public abstract Builder setOrg(String org);

        public abstract Builder setOrgId(@Nullable String id);

        public abstract Builder setJobTitle(String title);

        public abstract Builder setWebsite(String website);

        public abstract Builder setCountry(Country country);

        public abstract Builder setBio(@Nullable String bio);

        public abstract Builder setInterests(List<String> interests);

        public abstract Builder setWorkingGroupsInterests(List<String> interests);

        public abstract Builder setForumsUrl(@Nullable String url);

        public abstract Builder setProjectsUrl(@Nullable String url);

        public abstract Builder setGerritUrl(@Nullable String url);

        public abstract Builder setMailinglistUrl(@Nullable String url);

        public abstract Builder setMpcFavoritesUrl(@Nullable String url);

        public abstract EfUser build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_EfUser_Eca.Builder.class)
    public abstract static class Eca {

        public abstract boolean getSigned();

        public abstract boolean getCanContributeSpecProject();

        public static Builder builder() {
            return new AutoValue_EfUser_Eca.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setSigned(boolean signed);

            public abstract Builder setCanContributeSpecProject(boolean canContribute);

            public abstract Eca build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_EfUser_FriendsData.Builder.class)
    public abstract static class FriendsData {

        public abstract int getFriendId();

        public static Builder builder() {
            return new AutoValue_EfUser_FriendsData.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setFriendId(int id);

            public abstract FriendsData build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_EfUser_Country.Builder.class)
    public abstract static class Country {

        @Nullable
        public abstract String getCode();

        @Nullable
        public abstract String getName();

        public static Builder builder() {
            return new AutoValue_EfUser_Country.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setCode(@Nullable String code);

            public abstract Builder setName(@Nullable String name);

            public abstract Country build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_EfUser_PublisherAgreement.Builder.class)
    public abstract static class PublisherAgreement {

        public abstract String getVersion();

        public static Builder builder() {
            return new AutoValue_EfUser_PublisherAgreement.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setVersion(String version);

            public abstract PublisherAgreement build();
        }
    }
}
