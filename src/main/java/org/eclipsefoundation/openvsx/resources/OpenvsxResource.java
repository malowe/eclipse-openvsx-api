/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.resources;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.exception.FinalForbiddenException;
import org.eclipsefoundation.openvsx.api.models.EfUser;
import org.eclipsefoundation.openvsx.request.OAuthFilter;

public abstract class OpenvsxResource {

    @ConfigProperty(name = "eclipse.openvsx.allowed-users")
    List<String> allowedUsers;

    @Context
    HttpServletRequest request;

    /**
     * Extracts the user from the request context in order to determine a user is
     * signed in. Throws a FinalForbiddenException if there is no user associated
     * with this token.
     * 
     * @return The token user.
     */
    EfUser getTokenUser() {
        EfUser tokenUser = (EfUser) request.getAttribute(OAuthFilter.TOKEN_USER);
        if (tokenUser == null) {
            throw new FinalForbiddenException("No user associated with this token");
        }
        return tokenUser;
    }

    /**
     * Checks whether the token user is in the listed allowed users or if the token
     * user is the same as the user in the request URL.
     * 
     * @param urlUsername The username in the request URL
     * @return True if current user can access endpoint
     */
    void checkIfAdminOrSelf(String urlUsername) {
        EfUser currentUser = getTokenUser();

        if (!urlUsername.equalsIgnoreCase(currentUser.getName())
                && allowedUsers.stream().noneMatch(email -> email.equalsIgnoreCase(currentUser.getMail()))) {
            throw new FinalForbiddenException(String.format("Can not access resources with username: %s", urlUsername));
        }
    }
}
