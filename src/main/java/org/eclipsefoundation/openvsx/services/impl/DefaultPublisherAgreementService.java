/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.services.impl;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.commons.lang3.ArrayUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.foundationdb.client.model.PeopleDocumentData;
import org.eclipsefoundation.openvsx.api.models.EfUser;
import org.eclipsefoundation.openvsx.api.models.ModLogHeaders;
import org.eclipsefoundation.openvsx.models.DocumentBody;
import org.eclipsefoundation.openvsx.models.PublisherAgreementData;
import org.eclipsefoundation.openvsx.namespace.OpenvsxModLogActions;
import org.eclipsefoundation.openvsx.services.FoundationOperationService;
import org.eclipsefoundation.openvsx.services.PublisherAgreementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

@ApplicationScoped
public class DefaultPublisherAgreementService implements PublisherAgreementService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultPublisherAgreementService.class);

    @ConfigProperty(name = "eclipse.openvsx.doc-id")
    String openvsxDocId;
    @ConfigProperty(name = "eclipse.openvsx.document-version", defaultValue = "1")
    Integer docVersion;

    @Inject
    FoundationOperationService foundationService;

    @Inject
    ObjectMapper objectMapper;

    @Override
    public Optional<PublisherAgreementData> getPublisherAgreementByUsername(String username) {
        Optional<PeopleDocumentData> mostRecent = foundationService.fetchMostRecentDocument(username);
        return mostRecent.isEmpty() || isdocumentExpired(mostRecent.get()) ? Optional.empty()
                : Optional.of(buildPublisherAgreement(mostRecent.get(), true));
    }

    @Override
    public Optional<PublisherAgreementData> createPublisherAgreement(EfUser user) {
        try {
            LOGGER.debug("Creating publisher agreement for user: {}", user.getName());

            ModLogHeaders headers = ModLogHeaders.builder()
                    .setModLogKey2(user.getName())
                    .setModLogAction(OpenvsxModLogActions.AGREEMENT_SIGNED)
                    .setModLogPerson(user.getName()).build();

            LOGGER.debug("Encoding publisher agreement document for user: {}", user.getName());

            // Create document from user data + request info. Then encode it
            String jsonDoc = objectMapper.writeValueAsString(DocumentBody.builder()
                    .setVersion(String.valueOf(docVersion))
                    .setGithubHandle(user.getGithubHandle())
                    .setUsername(user.getName())
                    .setFirstName(user.getFirstName())
                    .setLastName(user.getLastName())
                    .setMail(user.getMail())
                    .build());

            Optional<PeopleDocumentData> creationResult = foundationService.persistDocumentWithModLog(headers,
                    createDocumentSigningRequest(jsonDoc, user.getName()));

            // Convert to PublisherAgreementData if persistence successful
            return creationResult.isEmpty() ? Optional.empty()
                    : Optional.of(buildPublisherAgreement(creationResult.get(), false));

        } catch (Exception e) {
            LOGGER.error("Error while creating publisher agreement", e);
            foundationService.insertErrorSysModLog("_openvsx_publisher_agreement_create", user.getName());
            return Optional.empty();
        }
    }

    @Override
    public Optional<PeopleDocumentData> revokePublisherAgreement(PeopleDocumentData document, String currentUser) {
        try {
            LOGGER.debug("{} is revoking publisher agreement for user: {}", currentUser, document.getPersonID());

            // Update the doc with an expiry date
            PeopleDocumentData updated = PeopleDocumentData.builder()
                    .setPersonID(document.getPersonID())
                    .setDocumentID(document.getDocumentID())
                    .setVersion(document.getVersion())
                    .setEffectiveDate(document.getEffectiveDate())
                    .setReceivedDate(document.getReceivedDate())
                    .setScannedDocumentFileName(document.getScannedDocumentFileName())
                    .setScannedDocumentMime(document.getScannedDocumentMime())
                    .setScannedDocumentBLOB(document.getScannedDocumentBLOB())
                    .setScannedDocumentBytes(document.getScannedDocumentBytes())
                    .setComments(document.getComments())
                    .setExpirationDate(new Date())
                    .build();

            ModLogHeaders headers = ModLogHeaders.builder()
                    .setModLogKey2(document.getPersonID())
                    .setModLogAction(OpenvsxModLogActions.AGREEMENT_REVOKED)
                    .setModLogPerson(currentUser).build();

            return foundationService.persistDocumentWithModLog(headers, updated);

        } catch (Exception e) {
            LOGGER.error("Error while revoking publisher agreement", e);
            foundationService.insertErrorSysModLog("_openvsx_publisher_agreement_delete", document.getPersonID());
            return Optional.empty();
        }
    }

    /**
     * Checks if the document is expired. True if expired, false if not.
     * 
     * @param document The current document.
     * @return True if expired/invalid, false otherwise
     */
    private boolean isdocumentExpired(PeopleDocumentData document) {
        boolean isExpired = document.getExpirationDate() != null
                && document.getExpirationDate().before(Date.from(Instant.now()));
        if (isExpired) {
            LOGGER.warn("Most recent document is expired");
        }
        return isExpired;
    }

    /**
     * Creates an OpenVSX publisher agreement document signing request using the
     * given user. Builds and encodes the current Publisher Agreement from the
     * necessary fields.
     * 
     * @param jsonDoc  The encoded Document as a Json String
     * @param username The user signing the agreement
     * @return The constructed PeopleDocumentData object.
     */
    private PeopleDocumentData createDocumentSigningRequest(String jsonDoc, String username) {
        LOGGER.debug("Creating document signing request for user: {}", username);

        List<Byte> docAsBytes = Arrays.asList(ArrayUtils.toObject(jsonDoc.getBytes()));

        // Set date to UTC timezone when creating
        Date now = Date.from(DateTimeHelper.now().toInstant());

        return PeopleDocumentData.builder()
                .setPersonID(username)
                .setDocumentID(openvsxDocId)
                .setVersion(docVersion)
                .setEffectiveDate(now)
                .setReceivedDate(now)
                .setScannedDocumentFileName("openvsx-publisher-agreement.json")
                .setScannedDocumentMime("application/json")
                .setScannedDocumentBLOB(docAsBytes)
                .setScannedDocumentBytes(jsonDoc.length())
                .setComments("Generated by api.eclipse.org/openvsx/publisher_agreement")
                .build();
    }

    /**
     * Builds a PublisherAgreementData object using the given PeopleDocumentData
     * entity. Has a flag to offset the received date by one day to compensate for
     * the modification made by fdndb-api.
     * 
     * @param document   The given PeopleDocumentData entity
     * @param adjustTime A Flag to increment the received date by a day
     * @return A populated PublisherAgreementData object
     */
    private PublisherAgreementData buildPublisherAgreement(PeopleDocumentData document, boolean adjustTime) {

        // Conversion of List<Byte> to byte[] to allow conversion to string
        byte[] blob = document.getScannedDocumentBLOB() == null ? new byte[0]
                : ArrayUtils.toPrimitive(
                        document.getScannedDocumentBLOB().toArray(new Byte[document.getScannedDocumentBLOB().size()]));

        SimpleDateFormat effectiveDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat receivedDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        String receivedDate = "";

        // The 'received_date' column is only a 'date' type and gets rewinded a day when
        // adjusted for UTC time by fdndb-api. Flag should onlybe set on fetch requests
        if (adjustTime) {
            ZonedDateTime adjustedTime = ZonedDateTime.parse(DateTimeHelper.toRFC3339(document.getReceivedDate()))
                    .plusDays(1);
            receivedDate = receivedDateFormat.format(Date.from(adjustedTime.toInstant()));
        } else {
            receivedDate = receivedDateFormat.format(document.getReceivedDate());
        }

        return PublisherAgreementData.builder()
                .setPersonID(document.getPersonID())
                .setDocumentID(document.getDocumentID())
                .setVersion(Integer.toString((int) document.getVersion()))
                .setEffectiveDate(effectiveDateFormat.format(document.getEffectiveDate()))
                .setReceivedDate(receivedDate)
                .setScannedDocumentBlob(new String(blob))
                .setScannedDocumentMime(document.getScannedDocumentMime())
                .setScannedDocumentBytes(Integer.toString(blob.length))
                .setScannedDocumentFileName(document.getScannedDocumentFileName())
                .setComments(document.getComments())
                .build();
    }
}
