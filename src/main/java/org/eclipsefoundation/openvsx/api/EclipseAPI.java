/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.api;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.openvsx.api.models.EfUser;

@ApplicationScoped
@Produces("application/json")
@RegisterRestClient(configKey = "eclipse-api")
public interface EclipseAPI {

    /**
     * Searches for users that match the given uid.
     * 
     * @param bearerToken The bearer token
     * @param uid         The uid to search
     * @return A List of EfUser entities if they exist
     */
    @GET
    @Path("/account/profile")
    List<EfUser> getUsers(@HeaderParam("Authorization") String bearerToken, @QueryParam("uid") String uid);
}
