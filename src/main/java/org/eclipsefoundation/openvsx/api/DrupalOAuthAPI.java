/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.api;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.openvsx.api.models.DrupalOAuthData;

/**
 * Drupal OAuth2 token validation API binding
 */
@Path("oauth2")
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
@RegisterRestClient(configKey = "accounts-api")
public interface DrupalOAuthAPI {

    /**
     * Fetches metadata for the current token string. Includes scope and user id.
     * 
     * @param token The raw token value
     * @return A DrupalOAuthData object if the token is valid
     */
    @GET
    @Path("/tokens/{token}")
    DrupalOAuthData getTokenInfo(@PathParam("token") String token);
}