/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.services.impl;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.exception.FinalForbiddenException;
import org.eclipsefoundation.openvsx.api.DrupalOAuthAPI;
import org.eclipsefoundation.openvsx.api.EclipseAPI;
import org.eclipsefoundation.openvsx.api.models.DrupalOAuthData;
import org.eclipsefoundation.openvsx.api.models.EfUser;
import org.eclipsefoundation.openvsx.helpers.DrupalAuthHelper;
import org.eclipsefoundation.openvsx.services.DrupalOAuthService;
import org.eclipsefoundation.openvsx.services.OAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
public class DefaultDrupalOAuthService implements DrupalOAuthService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultDrupalOAuthService.class);

    @Inject
    @RestClient
    EclipseAPI eclipseAPI;

    @Inject
    @RestClient
    DrupalOAuthAPI drupalAuthAPI;

    @Inject
    OAuthService oauth;

    @Override
    public DrupalOAuthData validateTokenStatus(String token, List<String> validScopes, List<String> validClientIds) {
        try {
            LOGGER.debug("Validating token: {}", token);

            DrupalOAuthData tokenData = drupalAuthAPI.getTokenInfo(token);

            if (DrupalAuthHelper.isExpired(tokenData.getExpires())) {
                throw new FinalForbiddenException("The Authorization token is expired");
            }
            if (!DrupalAuthHelper.hasScopes(tokenData.getScope(), validScopes)) {
                throw new FinalForbiddenException(
                        "The Authorization token does not have the required permissions/scopes");
            }
            if (!DrupalAuthHelper.hasValidclientId(tokenData.getClientId(), validClientIds)) {
                throw new FinalForbiddenException("The Authorization token has an Invalid client_id");
            }

            return tokenData;
        } catch (Exception e) {
            LOGGER.error("Error validating token", e);
            throw new FinalForbiddenException("Error validating token");
        }
    }

    @Override
    public EfUser getTokenUserInfo(String uid) {
        try {
            LOGGER.debug("Fetching user info for  with uid: {}", uid);

            return eclipseAPI.getUsers(getBearerToken(), uid).get(0);
        } catch (Exception e) {
            LOGGER.error("Error getting user from token", e);
            throw new NotFoundException("User not found");
        }
    }

    /**
     * Builds a bearer token using the token generated by the Oauth server.
     * 
     * @return A fully formed Bearer token
     */
    private String getBearerToken() {
        return "Bearer " + oauth.getToken();
    }
}