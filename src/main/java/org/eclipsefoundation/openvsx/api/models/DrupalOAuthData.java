/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.api.models;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_DrupalOAuthData.Builder.class)
public abstract class DrupalOAuthData {

    public abstract String getClientId();

    @Nullable
    public abstract String getUserId();

    public abstract String getAccessToken();

    public abstract long getExpires();

    public abstract String getScope();

    public static Builder builder() {
        return new AutoValue_DrupalOAuthData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setClientId(String id);

        public abstract Builder setUserId(@Nullable String id);

        public abstract Builder setAccessToken(String token);

        public abstract Builder setExpires(long expires);

        public abstract Builder setScope(String scope);

        public abstract DrupalOAuthData build();
    }
}