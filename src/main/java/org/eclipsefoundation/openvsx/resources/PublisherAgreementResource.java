/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.resources;

import java.util.Optional;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.model.Error;
import org.eclipsefoundation.foundationdb.client.model.PeopleDocumentData;
import org.eclipsefoundation.openvsx.api.models.EfUser;
import org.eclipsefoundation.openvsx.models.AgreementSigningRequest;
import org.eclipsefoundation.openvsx.models.PublisherAgreementData;
import org.eclipsefoundation.openvsx.services.FoundationOperationService;
import org.eclipsefoundation.openvsx.services.PublisherAgreementService;

@Path("publisher_agreement")
public class PublisherAgreementResource extends OpenvsxResource {

    private static final String NOT_FOUND_MSG_FORMAT = "Unable to find agreement for user: %s";

    @ConfigProperty(name = "eclipse.openvsx.document-version", defaultValue = "1")
    Double docVersion;

    @Inject
    PublisherAgreementService agreementService;

    @Inject
    FoundationOperationService foundationService;

    @GET
    public Response getAgreement() {

        // Uses currently logged in user. Only an onwer can fetch their agreement
        String username = getTokenUser().getName();

        Optional<PublisherAgreementData> result = agreementService.getPublisherAgreementByUsername(username);
        if (result.isEmpty()) {
            throw new NotFoundException(String.format(NOT_FOUND_MSG_FORMAT, username));
        }

        return Response.ok(result).build();
    }

    @POST
    public Response createAgreement(AgreementSigningRequest body) {

        // Uses currently logged in user. Only an owner can create their agreement
        EfUser user = getTokenUser();

        // Check if body format is correct
        validateSigningRequest(body);

        // Conflict if already signed agreement that isn't expired
        if (agreementService.getPublisherAgreementByUsername(user.getName()).isPresent()) {
            return new Error(Status.CONFLICT,
                    "The request could not be completed due to a conflict with the current state of the resource.")
                    .asResponse();
        }

        // Create user in fdndb if they don't exist
        if (!foundationService.createDbUserIfNotFound(user)) {
            throw new ServerErrorException("Internal Server Error", 500);
        }

        Optional<PublisherAgreementData> result = agreementService.createPublisherAgreement(user);
        if (result.isEmpty()) {
            throw new BadRequestException("Unable to create Publisher Agreement with current request parameters");
        }

        return Response.ok(result.get()).build();
    }

    @GET
    @Path("{efUsername}")
    public Response getAgreementForUser(@PathParam("efUsername") String username) {

        // The owner or admin can retrieve a specific agreement
        checkIfAdminOrSelf(username);

        Optional<PublisherAgreementData> result = agreementService.getPublisherAgreementByUsername(username);
        if (result.isEmpty()) {
            throw new NotFoundException(String.format(NOT_FOUND_MSG_FORMAT, username));
        }

        return Response.ok(result).build();
    }

    @DELETE
    @Path("{efUsername}")
    public Response revokeAgreementForUser(@PathParam("efUsername") String username) {

        // The owner or admin can retrieve a specific agreement
        checkIfAdminOrSelf(username);

        Optional<PeopleDocumentData> fetchResult = foundationService.fetchMostRecentDocument(username);
        if (fetchResult.isEmpty()) {
            throw new NotFoundException(String.format(NOT_FOUND_MSG_FORMAT, username));
        }

        String currentUser = getTokenUser().getName();

        Optional<PeopleDocumentData> updateResult = agreementService.revokePublisherAgreement(fetchResult.get(),
                currentUser);
        if (updateResult.isEmpty()) {
            throw new ServerErrorException("Internal Server Error", 500);
        }

        return Response.noContent().build();
    }

    /**
     * Validates the incoming signing request by ensuring the fields are in valid
     * states, and that the user in the URL is the same as the one bound to the
     * token.
     * 
     * @param request The incoming singing request
     */
    private void validateSigningRequest(AgreementSigningRequest request) {
        if (!StringUtils.isNumeric(request.getVersion()) || Double.parseDouble(request.getVersion()) != docVersion) {
            throw new BadRequestException("The version of the agreement is missing or invalid");
        }

        if (StringUtils.isBlank(request.getGithubHandle())) {
            throw new BadRequestException("The github_handle is missing or invalid");
        }

        // Ensure GH handle from current user same as in request body.
        if (!StringUtils.equalsIgnoreCase(getTokenUser().getGithubHandle(), request.getGithubHandle())) {
            throw new BadRequestException("The github_handle does not match our records.");
        }
    }
}