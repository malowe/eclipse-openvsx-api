/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.resources;

import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import org.eclipsefoundation.openvsx.models.AgreementSigningRequest;
import org.eclipsefoundation.openvsx.test.helpers.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.templates.RestAssuredTemplates;
import org.eclipsefoundation.testing.templates.RestAssuredTemplates.EndpointTestCase;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;

@QuarkusTest
class PublisherAgreementResourceTest {
    public static final String BASE_URL = "publisher_agreement";
    public static final String USER_URL = BASE_URL + "/{efusername}";

    public static final Optional<Map<String, Object>> userCreds = Optional
            .of(Map.of("Authorization", "Bearer token2"));

    public static final Optional<Map<String, Object>> userNoDocCreds = Optional
            .of(Map.of("Authorization", "Bearer token5"));

    public static final Optional<Map<String, Object>> invalidCreds = Optional
            .of(Map.of("Authorization", "Bearer token1"));

    public static final Optional<Map<String, Object>> docCreateCreds = Optional
            .of(Map.of("Authorization", "Bearer token7"));

    public static final EndpointTestCase GET_CURRENT_SUCCESS = TestCaseHelper
            .prepareTestCase(BASE_URL, new String[] {}, SchemaNamespaceHelper.PUBLISHER_AGREEMENT_SCHEMA_PATH)
            .setHeaderParams(userCreds).build();

    public static final EndpointTestCase GET_CURRENT_NOT_FOUND = TestCaseHelper
            .prepareTestCase(BASE_URL, new String[] {}, SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(404).setHeaderParams(userNoDocCreds).build();

    public static final EndpointTestCase BAD_CREDS = TestCaseHelper
            .prepareTestCase(BASE_URL, new String[] {}, null).setStatusCode(403)
            .setHeaderParams(invalidCreds).build();

    public static final EndpointTestCase POST_CURRENT_CONFLICT = TestCaseHelper
            .prepareTestCase(BASE_URL, new String[] {}, null)
            .setStatusCode(409)
            .setHeaderParams(userCreds).build();

    public static final EndpointTestCase POST_CURRENT_INVALID_HANDLE = TestCaseHelper
            .prepareTestCase(BASE_URL, new String[] {}, SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(400).setHeaderParams(userCreds).build();

    public static final EndpointTestCase GET_USER_SUCCESS = TestCaseHelper
            .prepareTestCase(USER_URL, new String[] { "fakeuser" },
                    SchemaNamespaceHelper.PUBLISHER_AGREEMENT_SCHEMA_PATH)
            .setHeaderParams(userCreds).build();

    public static final EndpointTestCase GET_USER_NOT_FOUND = TestCaseHelper
            .prepareTestCase(USER_URL, new String[] { "name" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(404).setHeaderParams(userNoDocCreds).build();

    public static final EndpointTestCase FOR_USER_BAD_CREDS = TestCaseHelper
            .prepareTestCase(USER_URL, new String[] { "fakeuser" },
                    SchemaNamespaceHelper.PUBLISHER_AGREEMENT_SCHEMA_PATH)
            .setStatusCode(403).setHeaderParams(invalidCreds).build();

    public static final EndpointTestCase REVOKE_SUCCESS = TestCaseHelper
            .prepareTestCase(USER_URL, new String[] { "fakeuser" },
                    SchemaNamespaceHelper.PUBLISHER_AGREEMENT_SCHEMA_PATH)
            .setHeaderParams(userCreds)
            .setStatusCode(204).build();

    public static final EndpointTestCase REVOKE_NO_DOC = TestCaseHelper
            .prepareTestCase(USER_URL, new String[] { "name" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(404)
            .setHeaderParams(userNoDocCreds).build();

    public static final EndpointTestCase REVOKE_INVALID_USER = TestCaseHelper
            .prepareTestCase(USER_URL, new String[] { "other" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(403)
            .setResponseContentType(ContentType.JSON)
            .setHeaderParams(userCreds).build();

    @Inject
    ObjectMapper mapper;

    /*
     * GET CURRENT USER
     */
    @Test
    void testGet_currentUser_success() {
        RestAssuredTemplates.testGet(GET_CURRENT_SUCCESS);
    }

    @Test
    void testGet_currentUser_success_validateResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_CURRENT_SUCCESS);
    }

    @Test
    void testGet_currentUser_success_validateSchema() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_CURRENT_SUCCESS);
    }

    @Test
    void testGet_currentUser_failure_notFound() {
        RestAssuredTemplates.testGet(GET_CURRENT_NOT_FOUND);
    }

    @Test
    void testGet_currentUser_failure_notFound_validateResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_CURRENT_NOT_FOUND);
    }

    @Test
    void testGet_currentUser_failure_notFound_validateSchema() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_CURRENT_NOT_FOUND);
    }

    @Test
    void testGet_currentUser_failure_badCreds() {
        RestAssuredTemplates.testGet(BAD_CREDS);
    }

    /*
     * POST CURRENT USER
     */
    @Test
    void testPost_currentUser_success() {
        RestAssuredTemplates.testPost(
                TestCaseHelper
                        .prepareTestCase(BASE_URL, new String[] {},
                                SchemaNamespaceHelper.PUBLISHER_AGREEMENT_SCHEMA_PATH)
                        .setHeaderParams(docCreateCreds).build(),
                generateSigningSample("nodoc"));
    }

    @Test
    void testPost_currentUser_conflict() {
        RestAssuredTemplates.testPost(POST_CURRENT_CONFLICT, generateSigningSample("fakeuser"));
    }

    @Test
    void testPost_currentUser_conflict_validateResponseFormat() {
        RestAssuredTemplates.testPost_validateResponseFormat(POST_CURRENT_CONFLICT, generateSigningSample("fakeuser"));
    }

    @Test
    void testPost_currentUser_conflict_validateSchema() {
        RestAssuredTemplates.testPost_validateResponseFormat(POST_CURRENT_CONFLICT, generateSigningSample("fakeuser"));
    }

    @Test
    void testPost_currentUser_failure_invalidRequestFormat() {
        RestAssuredTemplates.testPost(
                TestCaseHelper.prepareTestCase(BASE_URL, new String[] {}, null).setHeaderParams(userCreds)
                        .setRequestContentType(ContentType.TEXT).setStatusCode(500).build(),
                generateSigningSample("fakeuser"));
    }

    @Test
    void testPost_currentUser_failure_invalidHandle() {
        RestAssuredTemplates.testPost(POST_CURRENT_INVALID_HANDLE, generateSigningSample("otheruser"));
    }

    @Test
    void testPost_currentUser_failure_invalidHandle_validateFormat() {
        RestAssuredTemplates.testPost_validateResponseFormat(POST_CURRENT_INVALID_HANDLE,
                generateSigningSample("otheruser"));
    }

    @Test
    void testPost_currentUser_failure_badCreds() {
        RestAssuredTemplates.testPost(BAD_CREDS, generateSigningSample("fakeuser"));
    }

    /*
     * GET FOR USER
     */
    @Test
    void testGet_getForUser_success() {
        RestAssuredTemplates.testGet(GET_USER_SUCCESS);
    }

    @Test
    void testGet_geFortUser_success_validateResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_USER_SUCCESS);
    }

    @Test
    void testGet_getForUser_success_validateSchema() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_USER_SUCCESS);
    }

    @Test
    void testGet_getForUser_failure_notFound() {
        RestAssuredTemplates.testGet(GET_USER_NOT_FOUND);
    }

    @Test
    void testGet_getForUser_failure_notFound_validateResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_USER_NOT_FOUND);
    }

    @Test
    void testGet_getForUser_failure_notFound_validateSchema() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_USER_NOT_FOUND);
    }

    @Test
    void testGet_getForUser_failure_badCreds() {
        RestAssuredTemplates.testGet(FOR_USER_BAD_CREDS);
    }

    /*
     * DELETE FOR USER
     */
    @Test
    void testDelete_deleteForUser_success() {
        RestAssuredTemplates.testDelete(REVOKE_SUCCESS, null);
    }

    @Test
    void testDelete_deleteForUser_failure_invalidUser() {
        RestAssuredTemplates.testDelete(REVOKE_INVALID_USER, null);
    }

    @Test
    void testDelete_deleteForUser_failure_noDoc() {
        RestAssuredTemplates.testDelete(REVOKE_NO_DOC, null);
    }

    @Test
    void testDelete_deleteForUser_failure_noDoc_validateSchema() {
        RestAssuredTemplates.testDelete_validateSchema(REVOKE_NO_DOC, null);
    }

    @Test
    void testDelete_deleteForUser_failure_badCreds() {
        RestAssuredTemplates.testGet(FOR_USER_BAD_CREDS);
    }

    private String generateSigningSample(String ghHandle) {
        try {
            return mapper.writeValueAsString(
                    AgreementSigningRequest.builder().setVersion("1").setGithubHandle(ghHandle).build());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
