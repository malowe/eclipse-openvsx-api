/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.services;

import java.util.Optional;

import org.eclipsefoundation.foundationdb.client.model.PeopleDocumentData;
import org.eclipsefoundation.foundationdb.client.model.SysModLogData;
import org.eclipsefoundation.openvsx.api.models.EfUser;
import org.eclipsefoundation.openvsx.api.models.ModLogHeaders;

/**
 * Defines the service for performing specific CRUD operations on
 * FoundationDb-API.
 */
public interface FoundationOperationService {

    /**
     * Given the username, retrieves the most recent openvsx publisher
     * agreement for the given user.
     * 
     * @param username The desired user's EF username.
     * @return An Optional containing a PeopleDocumentData entity if it exists.
     */
    Optional<PeopleDocumentData> fetchMostRecentDocument(String username);

    /**
     * Sends a PeopleDocumentData object to the fdndb-api to be created/updated.
     * Sets a set of ModLog headers to enable auto-event logging at request-time.
     * 
     * @param headers  The request ModLog headers
     * @param toInsert the document to insert
     * @return An Optional containing the updated document if it exists.
     */
    Optional<PeopleDocumentData> persistDocumentWithModLog(ModLogHeaders headers, PeopleDocumentData toInsert);

    /**
     * Creates a SysModLogData entity using the desired values and sends it to
     * fdndb-api.
     * 
     * @param pk1      The given pk1
     * @param personId The person logging the event
     * @return An optional containing a SysModLogData entity if it exists
     */
    Optional<SysModLogData> insertErrorSysModLog(String pk1, String personId);

    /**
     * Checks if the given user exists in fdndb. Creates a new record if they can't
     * be found.
     * 
     * @param user The current user to add.
     * @return True if success. False if there was an error performing the
     *         operation.
     */
    boolean createDbUserIfNotFound(EfUser user);
}
