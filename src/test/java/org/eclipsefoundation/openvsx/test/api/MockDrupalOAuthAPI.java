/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.test.api;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.openvsx.api.DrupalOAuthAPI;
import org.eclipsefoundation.openvsx.api.models.DrupalOAuthData;

import io.quarkus.test.Mock;

@Mock
@RestClient
@ApplicationScoped
public class MockDrupalOAuthAPI implements DrupalOAuthAPI {

    List<DrupalOAuthData> tokens;

    public MockDrupalOAuthAPI() {
        tokens = new ArrayList<>();
        tokens.addAll(Arrays.asList(
                DrupalOAuthData.builder()
                        .setAccessToken("token1")
                        .setClientId("client-id")
                        .setExpires(1674111182)
                        .setScope("read write")
                        .build(),
                DrupalOAuthData.builder()
                        .setAccessToken("token2")
                        .setClientId("test-id")
                        .setUserId("42")
                        .setExpires(Instant.now().getEpochSecond() + 20000)
                        .setScope("read write admin")
                        .build(),
                DrupalOAuthData.builder()
                        .setAccessToken("token3")
                        .setClientId("test-id")
                        .setExpires(1234567890)
                        .setScope("read admin")
                        .build(),
                DrupalOAuthData.builder()
                        .setAccessToken("token4")
                        .setClientId("client-id")
                        .setExpires(Instant.now().getEpochSecond() + 20000)
                        .setScope("read write")
                        .build(),
                DrupalOAuthData.builder()
                        .setAccessToken("token5")
                        .setClientId("test-id")
                        .setUserId("333")
                        .setExpires(Instant.now().getEpochSecond() + 20000)
                        .setScope("read write admin")
                        .build(),
                DrupalOAuthData.builder()
                        .setAccessToken("token6")
                        .setClientId("test-id")
                        .setExpires(Instant.now().getEpochSecond() + 20000)
                        .setScope("read write admin")
                        .build(),
                DrupalOAuthData.builder()
                        .setAccessToken("token7")
                        .setClientId("test-id")
                        .setUserId("444")
                        .setExpires(Instant.now().getEpochSecond() + 20000)
                        .setScope("read write admin")
                        .build()));
    }

    @Override
    public DrupalOAuthData getTokenInfo(String token) {
        return tokens.stream().filter(t -> t.getAccessToken().equalsIgnoreCase(token)).findFirst().orElse(null);
    }
}