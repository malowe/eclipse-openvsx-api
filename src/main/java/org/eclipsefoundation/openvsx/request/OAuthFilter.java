/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.request;

import java.io.IOException;
import java.util.List;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.openvsx.api.models.DrupalOAuthData;
import org.eclipsefoundation.openvsx.helpers.DrupalAuthHelper;
import org.eclipsefoundation.openvsx.services.DrupalOAuthService;
import org.jboss.resteasy.util.HttpHeaderNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class OAuthFilter implements ContainerRequestFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(OAuthFilter.class);

    public static final String TOKEN_USER = "token_user";
    public static final String TOKEN_STATUS = "token_status";

    @ConfigProperty(name = "eclipse.openvsx.scopes", defaultValue = "openvsx_publisher_agreement")
    Instance<List<String>> validScopes;

    @ConfigProperty(name = "eclipse.openvsx.clients")
    Instance<List<String>> validClientIds;

    @ConfigProperty(name = "eclipse.openvsx.oauth-filter.enabled", defaultValue = "false")
    Instance<Boolean> isEnabled;

    @Inject
    DrupalOAuthService oauthService;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        if (Boolean.TRUE.equals(isEnabled.get())) {

            LOGGER.debug("Parsing auth header");
            String token = DrupalAuthHelper
                    .stripBearerToken(requestContext.getHeaderString(HttpHeaderNames.AUTHORIZATION));

            DrupalOAuthData tokenStatus = oauthService.validateTokenStatus(token, validScopes.get(),
                    validClientIds.get());
            if (tokenStatus != null) {

                // Set token data into context
                requestContext.setProperty(TOKEN_STATUS, tokenStatus);

                if (tokenStatus.getUserId() != null) {

                    // Fetch user data using token uid and set in context
                    LOGGER.debug("Fetching user info for token");
                    requestContext.setProperty(TOKEN_USER, oauthService.getTokenUserInfo(tokenStatus.getUserId()));
                }
            }
        }
    }
}
